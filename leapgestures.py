import pickle
import pygame
import math
from itertools import izip
import collections
import sys
import thread
import time
import itertools
# Use Leap to use Leapmotion, use LeapDummy to use mouse
#import Leap
import LeapDummy as Leap

# PyGame identifier for Leap Motion events
LEAP = pygame.USEREVENT+1


# Allows to iterate a flat list as pairs
def pairwise(t):
    it = iter(t)
    return izip(it,it)


class SampleListener(Leap.Listener):

    def on_init(self, controller):
        print "Initialized"

    def on_connect(self, controller):
        print "Connected"

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):
        frame = controller.frame()

        # Get hands and post position as pygame event
        for hand in frame.hands:
          if hand.palm_position:
            my_event = pygame.event.Event(LEAP, message=[hand.palm_position.x,hand.palm_position.y])
            pygame.event.post(my_event)


def normalize(coords):

  # Get min/max values
  x_values = []
  y_values = []
  for c in pairwise(coords):
    x_values.append(c[0])
    y_values.append(c[1])

  min_x = min(x_values)
  min_y = min(y_values)
  max_x = max(x_values) - min_x
  max_y = max(y_values) - min_y

  new_coords = []
  known = []

  for c in pairwise(coords):

    # Normalize origin to 0,0
    new_xvalue = (float(c[0]) - min_x)
    new_yvalue = (float(c[1]) - min_y)

    # Normalize scale to 15x15
    new_xvalue = int((new_xvalue / float(max_x)) * 15.0)
    new_yvalue = int((new_yvalue / float(max_y)) * 15.0)

    # Encode x/y positions as single value
    new_value = new_yvalue * 15 + new_xvalue
    if new_value not in known:
      new_coords.append(new_value)
      known.append(new_value)

  return new_coords


def predict(inputvector, symbol1, symbol2, symbol1_name, symbol2_name):

  # Calculate probability that inputvector represents symbol1
  probability_symbol1 = 0.0
  for datapoint in inputvector:
    # Calculate the probability of the datapoint given symbol1
    Pba = float(symbol1.count(datapoint)) / float(len(symbol1))

    # Calculate the overall probability of the datapoint
    Pa = float(symbol1.count(datapoint) + symbol2.count(datapoint)) / float(len(symbol1) + len(symbol2))

    # Calculate the overall probability of symbol1
    Pb = float(len(symbol1)) / float(len(symbol1) + len(symbol2))

    Pab = (Pba * Pa) / Pb
    probability_symbol1 += Pab

  # Calculate probability that inputvector represents symbol2
  probability_symbol2 = 0.0
  for datapoint in inputvector:
    # Calculate the probability of the datapoint given symbol2
    Pba = float(symbol2.count(datapoint)) / float(len(symbol2))

    # Calculate the overall probability of the datapoint
    Pa = float(symbol1.count(datapoint) + symbol2.count(datapoint)) / float(len(symbol1) + len(symbol2))

    # Calculate the overall probability of symbol2
    Pb = float(len(symbol2)) / float(len(symbol1) + len(symbol2))

    Pab = (Pba * Pa) / Pb
    probability_symbol2 += Pab

  print probability_symbol1, probability_symbol2

  confidence = 0.0
  name = ''

  if probability_symbol1 < probability_symbol2:
    confidence = probability_symbol2 / probability_symbol1
    name = symbol2_name
  else:
    confidence = probability_symbol1 / probability_symbol1
    name = symbol1_name

  confidence = "{0:.3f}".format(confidence)
  return name, confidence


if __name__ == "__main__":

  # Make sure two training sets are supplied
  if len(sys.argv) != 3:
    print 'Usage: leapgestures.py set1.pickle set2.pickle'
    sys.exit()

  # Define pygame colors
  color_black = (0,0,0)
  color_white = (255,255,255)
  color_red = (255,0,0)
  color_green = (0,255,0)


  # Set up pygame
  pygame.init()
  screen = pygame.display.set_mode((1000,1000))
  font = pygame.font.SysFont("Frutiger", 50)
  screen.fill(color_black)
  pygame.display.update()

  # Status control variables
  running = True
  record = False

  # Temporary storage of coordinates
  coords = []
  prex = 0
  prey = 0

  # Set up Leap Motion
  listener = SampleListener()
  controller = Leap.Controller()
  controller.add_listener(listener)

  # Load training data
  datafile1 = sys.argv[1]
  datafile2 = sys.argv[2]
  symbol1_data = pickle.load(open(datafile1, "rb"))
  symbol2_data = pickle.load(open(datafile2, "rb"))
  symbol1_name = datafile1[0:datafile1.find('.')]
  symbol2_name = datafile2[0:datafile2.find('.')]


  # Normalize training data
  symbol1_data_normalized = []
  for symbol in symbol1_data:
    symbol1_data_normalized.append(normalize(symbol))

  symbol2_data_normalized = []
  for symbol in symbol2_data:
    symbol2_data_normalized.append(normalize(symbol))

  # Flatten datasets
  symbol1_data_normalized = list(itertools.chain.from_iterable(symbol1_data_normalized))
  symbol2_data_normalized = list(itertools.chain.from_iterable(symbol2_data_normalized))


  # Main loop
  while running:

    event = pygame.event.poll()

    # Closing the window end the app
    if event.type == pygame.QUIT:
      running = 0

    # Esc ends the app. space starts/ends recording data
    elif event.type == pygame.KEYDOWN:
      if event.key == pygame.K_ESCAPE:
        running = 0

      if event.key == pygame.K_SPACE:
        record = not record

        # If we just stopped record, use current data and predict the symbol
        if not record:
          coordinates_normalized = normalize(coords)
          result, confidence = predict(coordinates_normalized, symbol1_data_normalized, symbol2_data_normalized, symbol1_name, symbol2_name)

          # Draw normalized data used for prediction
          screen.fill(color_black)
          for coordinate in coordinates_normalized:
            y = math.floor(coordinate / 15)
            x = coordinate % 15
            y = y * 20
            x = x * 20
            pygame.draw.rect(screen, color_white, (x, y, 20, 20))
          pygame.display.flip()
          coords = []

          # Draw result as label
          label = font.render(result, 1, (255,255,255))
          screen.blit(label, (340, 130))
          pygame.display.update()


    # Store mouse coordinates if in recording mode
    elif event.type == pygame.MOUSEMOTION:
      x = event.pos[0]
      y = event.pos[1]

      # Draw coordinates in red if in recording mode
      if record:
        pygame.draw.circle(screen, color_red, event.pos, 3, 3)
        pygame.display.flip()
        coords.append(x)
        coords.append(y)
      else:
        pygame.draw.circle(screen, color_black, (prex,prey), 11, 10)
        pygame.draw.circle(screen, color_green, (x,y), 10, 5)
        pygame.display.flip()
        prex = x
        prey = y


    # Store leap motion coordinates if in recording mode
    elif event.type == LEAP:
      xf,yf = event.message

      # Adjust data to better match screen coordinate system for drawing
      x = int(xf) + 200
      y = 500-int(yf)

      # Draw coordinates in red if in recording mode
      if record:
        pygame.draw.circle(screen, color_red, (x,y), 3, 3)
        pygame.display.flip()
        coords.append(x)
        coords.append(y)
      else:
        pygame.draw.circle(screen, color_black, (prex,prey), 11, 10)
        pygame.draw.circle(screen, color_green, (x,y), 10, 5)
        pygame.display.flip()
        prex = x
        prey = y














