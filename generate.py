import pygame
import math
import random
import pickle
from itertools import izip
import sys
import thread
import time

# Use Leap to use Leapmotion, use LeapDummy to use mouse
#import Leap
import LeapDummy as Leap

# PyGame identifier for Leap Motion events
LEAP = pygame.USEREVENT+1

# Allows to iterate a flat list as pairs
def pairwise(t):
  it = iter(t)
  return izip(it,it)


class SampleListener(Leap.Listener):

    def on_init(self, controller):
        print "Initialized"

    def on_connect(self, controller):
        print "Connected"

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_frame(self, controller):
        frame = controller.frame()
        for hand in frame.hands:
          if hand.palm_position:
            my_event = pygame.event.Event(LEAP, message=[hand.palm_position.x,hand.palm_position.y])
            pygame.event.post(my_event)




if __name__ == "__main__":

  # Make sure a filename to store samples is given
  if len(sys.argv) < 2:
    print "Usage: generate.py <symbol name>"
    sys.exit()
  file_name = sys.argv[1]


  # Define pygame colors
  color_black = (0,0,0)
  color_white = (255,255,255)
  color_red = (255,0,0)
  running = True
  record = False
  collection = []
  coords = []


  # Set up pygame
  pygame.init()
  screen = pygame.display.set_mode((640,480))
  screen.fill(color_black)
  pygame.display.update()


  # Set up leap motion
  listener = SampleListener()
  controller = Leap.Controller()
  controller.add_listener(listener)

  # Main loop
  while running:
    event = pygame.event.poll()

    # Esc saves samples to file and exits
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_ESCAPE:
        pickle.dump(collection, open(file_name + ".pickle", "wb"))
        running = 0

      # space toggles recording mode
      if event.key == pygame.K_SPACE:
        record = not record

        # Reset if recording is stopped
        if not record:

          # Clear screen
          screen.fill(color_black)
          pygame.display.update()

          # Draw current dataset
          for n in pairwise(coords):
            pygame.draw.circle(screen, color_white, (int(n[0]), int(n[1])), 1, 1)
          pygame.display.flip()

          # Save dataset and clear buffer
          collection.append(coords)
          coords = []

    # Handle mouse input
    elif event.type == pygame.MOUSEMOTION:
      if record:
        pygame.draw.circle(screen, color_red, event.pos, 1, 1)
        pygame.display.flip()
        coords.append(event.pos[0])
        coords.append(event.pos[1])

    # Handle leap motion input
    elif event.type == LEAP:
      # Extract coordinates and transform to match pygame coordinate system
      xf,yf = event.message
      x = int(xf) + 200
      y = 500-int(yf)

      if record:
        pygame.draw.circle(screen, color_red, (x,y), 1, 1)
        pygame.display.flip()
        coords.append(x)
        coords.append(y)



























